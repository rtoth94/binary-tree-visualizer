from tkinter import *
import math
from random import randrange as rr
from tkinter import ttk
import webbrowser
from tkinter import messagebox


class Window:
    def __init__(self):
        self.root = Tk()  # 800x600
        self.root.wm_title("Binary Tree Visualizer v7.0 - Created by Tóth Richard")
        self.root.resizable(0, 0)  # Not resizable window

        # Draw out 3 MAIN FRAMES and canvas
        self.draw_frames()

        # send this
        self.data = None
        self.messagebox = None

        self.entryVar = StringVar()
        self.mes = StringVar()
        self.output = StringVar()
        self.inord = StringVar()
        self.postord = StringVar()
        self.n = IntVar()
        self.speed = IntVar()

    def draw_frames(self):
        ## Two main frames: Top frame and Bottom frame
        self.TopFrame = Frame(self.root, width=1000, height=850, bg="#656F73")
        self.TopFrame.pack(side='top')
        self.BottomFrame = Frame(self.root, width=1000, height=150, relief="sunken", bg="#656F73")
        self.BottomFrame.pack(side='bottom')

        ## Top left is canvas, Top right is the frame for buttons
        self.TopLeft = Frame(self.TopFrame, width=800, height=600, bg="#656F73")
        self.TopLeft.pack(side='left')

        self.TopRight = Frame(self.TopFrame, width=200, height=620, bg="#656F73", relief="raised")
        self.TopRight.pack(side='right')

        # Frame on Main Frame
        self.BotOnly = Frame(self.BottomFrame, width=1020, height=150, bd=5, relief="sunken")
        self.BotOnly.pack(side='bottom')

        # Upper part - Console/messages
        self.consoleframe = LabelFrame(self.BotOnly, text="Console", font='consolas 14 italic')
        self.consoleframe.pack()

        # Bottom part/the output = canvas have option to scroll
        self.outputframe = Canvas(self.BotOnly, bd=0, highlightthickness=0, scrollregion=(0, 0, 200, 200))

        vbar = Scrollbar(self.BotOnly, orient=VERTICAL)
        vbar.pack(side=RIGHT, fill=Y)
        vbar.config(command=self.outputframe.yview)

        self.outputframe.config(width=1000, height=65)
        self.consoleframe.config(width=1000, height=65)
        self.outputframe.config(yscrollcommand=vbar.set)
        self.outputframe.pack(fill="both", expand="yes")

        ## Canvas
        self.canvas = Canvas(self.TopLeft, width=800, height=600, bg="white", relief="flat", bd=8)
        self.canvas.pack(fill="both", expand=True)

    def add_labels(self, function1=None, function2=None, function3=None, function4=None, function5=None, menu1=None, menu2=None, function6=None,
                   function7=None):
        self.labelframe = LabelFrame(self.TopRight, text="The application controls")
        self.labelframe.pack(fill="both", expand="yes")
        self.labelframe.place(width=200, height=620)

        self.buttonlabel = Label(self.labelframe, text="Control buttons", anchor="n", pady=6)
        self.buttonlabel.pack()
        self.buttonlabel.place(width=180, height=80)

        self.button1 = Button(self.labelframe, text="Create tree", font="arial 10 bold", activebackground="#E0586A", command=function1)
        self.button1.pack()
        self.button1.place(width=80, height=40, x=10, y=30)

        self.button2 = Button(self.labelframe, text="Clear", font="arial 10 bold", activebackground="#E0586A", command=function2)
        self.button2.pack()
        self.button2.place(width=80, height=40, x=110, y=30)

        self.button3 = Button(self.labelframe, text="Gernerate random", font="arial 8 bold", activebackground="#E0586A", command=function3)
        self.button3.pack()
        self.button3.place(width=120, height=40, x=10, y=195)

        self.entry2 = Entry(self.labelframe, bd=4, textvariable=self.n)
        self.entry2.pack()
        self.entry2.place(x=135, y=200, width=45, height=30)
        self.n.set(10)  ## Default

        self.entrylabel = Label(self.labelframe, text="Edit nodes", anchor="n", pady=6)
        self.entrylabel.pack()
        self.entrylabel.place(width=180, height=30, y=80)

        self.entrylabel2 = Label(self.labelframe, text="Value:", font="arial 10 bold")
        self.entrylabel2.pack()
        self.entrylabel2.place(y=110, x=5)

        self.entry1 = Entry(self.labelframe, bd=5, textvariable=self.entryVar)
        self.entry1.pack()
        self.entry1.place(y=110, x=55)

        self.vis_label = Label(self.labelframe, text="Algorithm Visualizer", anchor="n", pady=4)
        self.vis_label.pack()
        self.vis_label.place(width=180, height=80, y=245)

        self.box_value = StringVar()
        self.box = ttk.Combobox(self.labelframe, textvariable=self.box_value, state='readonly')
        self.box['values'] = ("In-Order", 'Post-Order', 'Pre-Order', "Level-Order")
        self.box.current(0)
        self.box.place(x=10, y=280, width=100)

        self.speed_label = Label(self.labelframe, text="Animation speed:\n 0 = Fastest \n 1000 = Slowest", font="arial 9", anchor="w", pady=5)
        self.speed_label.pack()
        self.speed_label.place(width=180, height=40, y=310, x=10)

        self.entry3 = Entry(self.labelframe, bd=4, textvariable=self.speed)
        self.entry3.pack()
        self.entry3.place(x=130, y=330, width=60, height=30)
        self.speed.set(400)  ## Default

        self.button4 = Button(self.labelframe, text="Start", font="arial 10 bold", activebackground="#E0586A", command=function4)
        self.button4.pack()
        self.button4.place(width=60, height=40, x=125, y=280)

        self.button5 = Button(self.labelframe, text="Stop", font="arial 10 bold", activebackground="#E0586A", command=function5)
        self.button5.pack()
        self.button5.place(width=180, height=30, x=5, y=378)

        ## Need this to send
        self.data = self.entry1
        ###################################################################################################################
        self.recreator = Label(self.labelframe, text="Tree Recreator", font="arial 10", pady=5)
        self.recreator.pack()
        self.recreator.place(width=180, height=40, y=410, x=10)

        self.button6 = Button(self.labelframe, text="Recreate", font="arial 10 bold", activebackground="#E0586A", command=function6)
        self.button6.pack()
        self.button6.place(width=90, height=30, x=5, y=550)

        self.button7 = Button(self.labelframe, text="Generate", font="arial 10 bold", activebackground="#E0586A", command=function7)
        self.button7.pack()
        self.button7.place(width=90, height=30, x=100, y=550)

        ########### Entry 1
        self.inorder_label = Label(self.labelframe, text="In-order:", font="arial 8 bold")
        self.inorder_label.pack()
        self.inorder_label.place(y=440, x=10)
        ###
        self.entry_inorder = Entry(self.labelframe, bd=4, textvariable=self.inord)
        self.entry_inorder.pack()
        self.inord.set("45,33,24,44,5,38,41,24,34,27")
        self.entry_inorder.place(x=10, y=460, width=180, height=30)

        ############ Entry 2

        self.post_label = Label(self.labelframe, text="Post-order:", font="arial 8 bold")
        self.post_label.pack()
        self.post_label.place(y=490, x=10)
        ###
        self.entry_postorder = Entry(self.labelframe, bd=4, textvariable=self.postord)
        self.entry_postorder.pack()
        self.postord.set("33,45,44,24,38,24,41,27,34,5")
        self.entry_postorder.place(x=10, y=510, width=180, height=30)

        ###################################################################################################################

        self.messagebox = Message(self.BotOnly, textvariable=self.mes, font='consolas 16', anchor="w", width=990)
        self.messagebox.pack()
        self.mes.set("")
        self.messagebox.place(width=980, height=30, y=25, x=5)

        self.output_mes = Message(self.outputframe, textvariable=self.output, font='consolas 16', anchor="w", width=800)
        self.output_mes.pack()
        self.output.set("")

        self.hinttext = "You can edit Nodes with double click. - Highlight"
        self.hint = Message(self.labelframe, text=self.hinttext, font='consolas 10 italic', anchor="w", width=180)
        self.hint.pack()
        self.hint.place(height=35, x=5, y=150)

        ##############################################################################################################################

        # Create a menubar
        self.menubar = Menu(self.root)
        # Generate Submenus
        self.filemenu = Menu(self.menubar, tearoff=0)
        self.filemenu.add_command(label="Save Tree", state=DISABLED)
        self.filemenu.add_command(label="Load tree", state=DISABLED)
        self.filemenu.add_separator()
        self.filemenu.add_command(label="Exit", command=self.root.quit)
        ####################################################################
        self.helpmenu = Menu(self.menubar, tearoff=0)
        self.helpmenu.add_command(label="Visit app's webpage", command=menu1)
        self.helpmenu.add_command(label="How to use")
        ####################################################################
        self.about = Menu(self.menubar, tearoff=0)
        self.about.add_command(label="About me", command=menu2)
        ## Add submenus to menubar
        self.menubar.add_cascade(label="File", menu=self.filemenu)
        self.menubar.add_cascade(label="Help", menu=self.helpmenu)
        self.menubar.add_cascade(label="About", menu=self.about)

        self.root.config(menu=self.menubar)
        ###################################################################################################################################


class Tree:
    class Node:
        def __init__(self, data, x, y, canvas, left=None, right=None):
            self.r = 15
            self.x = x
            self.y = y
            self.left = left
            self.right = right
            self.data = data
            self.id = canvas.create_oval(x + self.r, y + self.r, x - self.r, y - self.r, fill="lightgray")
            self.t = canvas.create_text(x, y, text=data, font='consolas 12 bold')
            self.indicator = False

    def __init__(self, canvas):
        self.new = None
        self.canvas = canvas
        self.r = 15
        self.level = 1
        self.offset = 40
        self.start_x = 400
        self.fill = 1  # New nodes automatically fills with numbers
        self.length = set()
        self.highlight = False

        # Click to create, double click to edit
        self.draw_bind = self.canvas.bind("<Button-1>", self.control)
        self.editclick = self.canvas.bind("<Double-Button-1>", self.edit)

        # Data got from class Program with function recieve
        self.entry = None
        self.entryVar = None
        self.mes = None

    def recieve(self, data, data2, data3):
        self.entry = data
        self.entryVar = data2
        self.mes = data3

    # Creating the starting nodes
    def create_first(self):
        ## Lines, root, and 2 Ghost-Node creation
        self.canvas.create_line(self.start_x, self.offset, self.start_x - self.start_x // 2, 2 * self.offset, fill="lightgrey")
        self.canvas.create_line(self.start_x, self.offset, self.start_x + self.start_x // 2, 2 * self.offset, fill="lightgrey")
        self.new = self.Node(0, self.start_x, self.level * self.offset, self.canvas)
        self.level += 1
        self.new.left = self.Node(None, self.start_x - self.start_x // self.level, self.level * self.offset, self.canvas)
        self.new.right = self.Node(None, self.start_x + self.start_x // self.level, self.level * self.offset, self.canvas)
        self.canvas.itemconfig(self.new.left.id, fill="white", outline="lightgrey")
        self.canvas.itemconfig(self.new.right.id, fill="white", outline="lightgrey")

    def create_others(self, root, x1, x2, y, data):
        # x1 = right
        # x2 = left
        # Change clicked Node
        root.data = data

        self.canvas.create_line(root.x + self.r, root.y + self.r // 2, x1, y, fill="lightgrey")
        self.canvas.create_line(root.x - self.r, root.y + self.r // 2, x2, y, fill="lightgrey")

        self.canvas.itemconfig(root.t, text=data)
        self.canvas.itemconfig(root.id, fill="lightgray", outline="black")

        ## Add 2 Ghost-Nodes
        root.right = self.Node(None, x1, y, self.canvas)
        self.canvas.itemconfig(root.right.id, fill="white", outline="lightgrey")
        root.left = self.Node(None, x2, y, self.canvas)
        self.canvas.itemconfig(root.left.id, fill="white", outline="lightgrey")

    def control(self, event):

        def search(node, sir):
            if node is None:
                return
            a = math.pow((event.x - node.x), 2)
            b = math.pow((event.y - node.y), 2)
            distance = math.sqrt(a + b)
            # using d = sqrt((x1-x2)^2 + (y1-y2)^2)

            if distance <= self.r and node.data != None:
                # just displaying the value in Node
                if self.highlight is not True:
                    self.entry.delete(0, 30)  # delete existing value
                    self.entry.insert(10, node.data)  # insert node da

            # Creating new Ghost-Nodes
            if distance <= self.r and node.data == None and self.highlight is False:
                self.mes.set("Added node!")
                self.entry.delete(0, 30)  # delete existing value
                self.create_others(node, node.x + sir // 2, node.x - sir // 2, node.y + self.offset, self.fill)
                self.fill += 1

            search(node.left, sir // 2)
            search(node.right, sir // 2)

        search(self.new, 400)

    def clear_length(self, node):
        if node is None:
            return 0
        if node.left.data != None:
            self.length.add(node.left.data)
            self.clear_length(node.left)
        if node.right.data != None:
            self.length.add(node.right.data)
            self.clear_length(node.right)
        return 1 + len(self.length)

    def edit(self, event):
        def search(node):
            if node is None:
                return
            a = math.pow((event.x - node.x), 2)
            b = math.pow((event.y - node.y), 2)
            distance = math.sqrt(a + b)

            # if I doubleclick into the Node with data, I can edit it, IF it is highlighted
            if distance <= self.r and node.data != None:

                if self.highlight is not True and node.indicator is not True:
                    self.canvas.itemconfig(node.id, outline="green", width=3)
                    self.highlight = True
                    node.indicator = True
                    self.entryVar.trace("w", lambda name, index, mode, sv=self.entryVar: self.change(sv, node))
                    self.h_id = node.id
                    self.mes.set("Node highlighted!")
                elif self.highlight is True and node.indicator is True:
                    self.canvas.itemconfig(node.id, outline="black", width=1)
                    self.highlight = False
                    node.indicator = False
                    self.mes.set("Highlight canceled!")

            search(node.left)
            search(node.right)

        search(self.new)

    def change(self, sv, node):
        ## Editable if its highlighted
        if node.indicator is not True:
            return
        else:
            ## get data from entry, change on canvas, and in tree code
            new = sv.get()
            self.canvas.itemconfig(node.t, text=new)
            node.data = new
            self.mes.set("New value set: " + str(new))


class Program:
    class GeneratedNode:
        def __init__(self, data, left=None, right=None):
            self.data = data
            self.left = left
            self.right = right
            self.x = 0
            self.y = 0
            self.indicator = False
            self.id = 0
            self.t = 0
            self.square = 0
            self.square2 = 0

        def inbox(self, canvas, i):
            x = (50 * i) + 50
            y = 450
            a = 50
            self.square = canvas.create_rectangle(x, y, x + a, y + a, fill='azure')
            canvas.create_text(x + 25, y + 25, text=str(self.data), font='arial 20 bold')

        def postbox(self, canvas, j):
            x = (50 * j) + 50
            y = 510
            a = 50
            self.square2 = canvas.create_rectangle(x, y, x + a, y + a, fill='azure')
            canvas.create_text(x + 25, y + 25, text=str(self.data), font='arial 20 bold')

    def __init__(self):
        self.window = Window()  ## Start GUI
        self.canvas = self.window.canvas  ## Call canvas into main class
        self.t = Tree(self.canvas)  ## Instance to tree
        self.creator = False  ## Only one time can Start

        # Create main Widgets
        self.window.add_labels(self.start, self.clear, self.generate, self.visualizer, self.stop, self.webpage, self.about, self.recreate,
                               self.generate_orders)

        ## Datas
        self.n = 10
        self.time = 400
        self.s = 1
        self.defaultbg = self.window.outputframe.cget('bg')

        ## Node counter entry and his tracing
        self.en = self.window.n
        self.en.trace("w", lambda name, index, mode, sv=self.en: set(sv))

        ## Speed counter and tracing
        self.te = self.window.speed
        self.te.trace("w", lambda name, index, mode, sv2=self.te: set2(sv2))

        ## RECREATED TREE
        self.c_tree = None

        def set(sv):
            try:
                new = sv.get()
                self.n = new
            except:
                self.n = 10

        def set2(sv2):
            try:
                new = sv2.get()
                self.time = new
            except:
                self.time = 400
                self.window.mes.set("Set to DEFAULT 400!")
                self.window.messagebox.after(3000, self.messagebox_disappear)

        # Sending data to other class
        self.send_datas()
        self.canvas.mainloop()

    def start(self):
        self.window.messagebox.after(3000, self.messagebox_disappear)
        self.clear()
        if self.creator is not True:
            self.window.mes.set("Root node created!")
            self.t.create_first()
            self.creator = True

    def clear(self):
        self.window.messagebox.after(3000, self.messagebox_disappear)
        self.window.inord.set("")
        self.window.postord.set("")
        self.canvas.delete("all")  ##Console
        ##Reset counters
        self.t.new = None
        self.creator = False
        self.t.level = 1
        self.t.fill = 1
        self.s = 1
        self.window.entry1.delete(0, 30)
        self.window.messagebox.config(bg=self.defaultbg)
        self.window.mes.set("Window cleared!")
        self.window.output.set("")

    def generate(self):
        self.window.messagebox.after(3000, self.messagebox_disappear)
        self.clear()
        self.values = []
        self.t.new = self.GeneratedNode(rr(20))

        self.window.mes.set("Generated random tree with {} nodes".format(self.n))

        def generate_inside(node, value):
            if rr(2) == 0:
                if node.left is None:
                    node.left = self.GeneratedNode(value)
                else:
                    generate_inside(node.left, value)
            else:
                if node.right is None:
                    node.right = self.GeneratedNode(value)
                else:
                    generate_inside(node.right, value)

        for i in range(self.n - 1):
            generate_inside(self.t.new, rr(30))

        def draw_new(node, ss, x, y):
            if node is None:
                return
            if node.left is not None:
                self.canvas.create_line(x, y, x - ss // 2, y + 40)
                draw_new(node.left, ss // 2, x - ss // 2, y + 40)
            if node.right is not None:
                self.canvas.create_line(x, y, x + ss // 2, y + 40)
                draw_new(node.right, ss // 2, x + ss // 2, y + 40)
            object = self.canvas.create_oval(x - 15, y - 15, x + 15, y + 15, fill='lightgray', outline='')
            ## Setting additional information to GeneratedNodes, to able to edit, and draw out
            node.x = x
            node.y = y
            node.id = object
            text = self.canvas.create_text(x, y, text=node.data, font='consolas 12 bold')
            node.t = text

        draw_new(self.t.new, 400, 400, 40)

    def normalize(self, node):
        def normalize_rek(node):
            if node is None:
                return
            normalize_rek(node.left)
            if node.data != None:
                self.canvas.itemconfig(node.id, fill="lightgrey")
            normalize_rek(node.right)

        normalize_rek(self.t.new)

    def visualizer(self):
        self.window.messagebox.after(5000, self.messagebox_disappear)

        ## Help variable to output message
        self.h = ""

        def write_inorder():

            self.normalize(self.t.new)

            def inorder_rek(node):
                if node is None:
                    return
                inorder_rek(node.left)
                if node.data != None and self.s == 1:
                    self.canvas.itemconfig(node.id, fill="#CCC860")
                    self.running_id = self.canvas.after(self.time)
                    self.canvas.update()
                    self.h += str(node.data) + " "
                    self.window.output.set("In-Order = " + self.h)
                inorder_rek(node.right)

            inorder_rek(self.t.new)

        def write_postorder():
            self.normalize(self.t.new)

            def postorder_rek(node):
                if node is None:
                    return
                postorder_rek(node.left)
                postorder_rek(node.right)
                if node.data != None and self.s == 1:
                    self.canvas.itemconfig(node.id, fill="#33A650")
                    self.running_id = self.canvas.after(self.time)
                    self.canvas.update()
                    self.h += str(node.data) + " "
                    self.window.output.set("Post-Order = " + self.h)

            postorder_rek(self.t.new)

        def write_preorder():

            self.normalize(self.t.new)

            def preorder_rek(node):
                if node.data != None and self.s == 1:
                    self.canvas.itemconfig(node.id, fill="#E65563")
                    self.running_id = self.canvas.after(self.time)
                    self.canvas.update()
                    self.h += str(node.data) + " "
                    self.window.output.set("Pre-Order = " + self.h)
                if node.left is not None:
                    preorder_rek(node.left)
                if node.right is not None:
                    preorder_rek(node.right)

            preorder_rek(self.t.new)

        def write_levelorder():
            self.normalize(self.t.new)
            q = [(self.t.new, 0)]
            result = 0
            while q != []:
                node, level = q.pop(0)
                if result != level:
                    self.h += "++ "
                if node.data != None and self.s == 1:
                    self.canvas.itemconfig(node.id, fill="#D159DE")
                    self.running_id = self.canvas.after(self.time)
                    self.canvas.update()
                    self.h += str(node.data) + " "
                    self.window.output.set("Level-Order = " + self.h)
                result = level
                if node.left is not None:
                    q.append((node.left, level + 1))
                if node.right is not None:
                    q.append((node.right, level + 1))

        if self.t.new is not None and self.time <= 1000:
            ## Cannot call if speed is too big or root is empty
            selected = self.window.box.get()
            self.window.mes.set("Vizualizing {} algorithm with speed {}!".format(selected, self.time))
            self.s = 1

            if selected == "In-Order":
                write_inorder()
            elif selected == "Pre-Order":
                write_preorder()
            elif selected == "Post-Order":
                write_postorder()
            elif selected == "Level-Order":
                write_levelorder()
        else:
            self.window.mes.set("No tree to visualize OR Bad speed value!")
            self.window.messagebox.config(bg="red")

    def stop(self):
        ## Indicator to stop process
        self.s = 2

        self.window.messagebox.config(bg="red")
        self.window.mes.set("Process Stopped")
        self.window.messagebox.after(3000, self.messagebox_disappear)
        self.window.messagebox.config(bg=self.defaultbg)

    def messagebox_disappear(self):
        self.window.messagebox.config(bg=self.defaultbg)
        self.window.mes.set("")

    def webpage(self):
        self.window.mes.set("Visiting application page!")
        self.window.messagebox.after(4000, self.messagebox_disappear)
        webbrowser.open_new("http://davinci.fmph.uniba.sk/~toth145/RocnikovyProjekt/")

    def about(self):
        about_message = "Author: Tóth Richárd \n" \
                        "FMFI UK - Aplikovaná informatika \n" \
                        "E-mail: rtoth94@gmail.com \n" \
                        "App version: 7.0 \n"

        self.top = Toplevel(width=300, height=300)
        self.top.resizable(0, 0)  # Not resizable window
        self.top.title("About me...")
        self.msg = Message(self.top, text=about_message, font="arial 18", width=400)
        self.msg.pack()

    def send_datas(self):
        # Sending: entry, and entryVar, message_entry
        self.t.recieve(self.window.data, self.window.entryVar, self.window.mes)

    ##############################################################################################################################
    ##############################################################################################################################

    def generate_orders(self):
        self.in_generated = ""
        self.post_generated = ""
        self.values = []
        self.clear()
        self.window.mes.set("Generated random orders!")
        self.t.new = self.first = self.GeneratedNode(rr(50))

        def generate_inside(node, value):

            if rr(2) == 0:
                if node.left is None:
                    node.left = self.GeneratedNode(value)
                else:
                    generate_inside(node.left, value)
            else:
                if node.right is None:
                    node.right = self.GeneratedNode(value)
                else:
                    generate_inside(node.right, value)

        i = 0
        while i < 10 - 1:
            v = rr(50)
            if v not in self.values or v != self.first:
                generate_inside(self.t.new, v)
                i += 1

        def inorder_rek(node):
            if node is None:
                return
            inorder_rek(node.left)
            if node.data != None and self.s == 1:
                self.in_generated += str(node.data) + ","
            inorder_rek(node.right)

        inorder_rek(self.t.new)

        def postorder_rek(node):
            if node is None:
                return
            postorder_rek(node.left)
            postorder_rek(node.right)
            if node.data != None and self.s == 1:
                self.post_generated += str(node.data) + ","

        postorder_rek(self.t.new)

        ## Set entry Values
        self.window.inord.set(self.in_generated[:-1])
        self.window.postord.set(self.post_generated[:-1])

    def recreate(self):
        ## Convert entry items into int
        try:
            self.inorder = list(map(int, self.window.inord.get().strip().split(',')))
            self.postorder = list(map(int, self.window.postord.get().strip().split(',')))
            ## Control the input
            self.controls(self.inorder, self.postorder)

            ##################### Got proper orders ######################################

            def buildTree(inorder, postorder):
                if len(inorder) == 0:
                    return None
                if len(inorder) == 1:
                    k = self.GeneratedNode(inorder[0])
                    return k
                root = self.GeneratedNode(postorder[- 1])
                index = inorder.index(postorder[-1])
                ##left subtree: In-order(from 0 to index), Post-order(from 0 to index == cut last element [:-1])
                root.left = buildTree(inorder[0: index], postorder[0: index])
                ##rigth subrtree: In=order(from index to last element), Post-order(from index to last element -1)
                root.right = buildTree(inorder[index + 1: len(inorder)], postorder[index: len(postorder) - 1])
                return root

            self.c_tree = buildTree(self.inorder, self.postorder)

            ######################## Got the tree, run order algorithms to draw out visualization tool to canvas ###############################

            def boxes(tree):
                self.i = 0

                def inorder_rek(node):
                    if node is None:
                        return
                    inorder_rek(node.left)
                    if self.s == 1:
                        node.inbox(self.canvas, self.i)
                        self.i += 1
                    inorder_rek(node.right)

                #######################################
                inorder_rek(tree)

                self.j = 0

                def postorder_rek(node):
                    if node is None:
                        return
                    postorder_rek(node.left)
                    postorder_rek(node.right)
                    if self.s == 1:
                        node.postbox(self.canvas, self.j)
                        self.j += 1

                #########################################
                postorder_rek(tree)

            boxes(self.c_tree)
        except:
            print("Entry error!")

        ######################## Draw out the recreated tree slightly ###############################

        def draw_recreated(node, ss, x, y):
            if node is None:
                return
            if node.left is not None:
                self.canvas.create_line(x, y, x - ss // 2, y + 40, fill="lightgrey")
                draw_recreated(node.left, ss // 2, x - ss // 2, y + 40)
            if node.right is not None:
                self.canvas.create_line(x, y, x + ss // 2, y + 40, fill="lightgrey")
                draw_recreated(node.right, ss // 2, x + ss // 2, y + 40)
            object = self.canvas.create_oval(x - 15, y - 15, x + 15, y + 15, fill='white', outline='lightgrey')
            node.id = object
            text = self.canvas.create_text(x, y, text=node.data, font='consolas 12', fill='lightgrey')
            node.t = text

        draw_recreated(self.c_tree, 400, 400, 40)

        ############################## Call visualization ###########################################

        self.visualize(self.c_tree, self.canvas)

    def visualize(self, tree, canvas):

        def reverse_preord(node, con, con2):
            if self.s == 1:
                if node is None:
                    return
                canvas.itemconfig(node.square2, fill="lightgrey")

                ###########################################################
                def sub(nod, color):
                    if nod is None:
                        return
                    
                    canvas.itemconfig(nod.id, fill=color)
                    
                    if nod.left is not None:
                        sub(nod.left, color)
                        
                    if nod.right is not None:
                        sub(nod.right, color)
                
                sub(node.left, 'orange')
                sub(node.right, 'lightblue')
                
                ############################################################
                if len(con) == 0:
                    self.canvas.itemconfig(node.id, fill="green")
                    self.canvas.itemconfig(node.t, fill="black")
                    con.append((node.id, node.t))
                    ####################################################################
                    con2.append(node.square)
                else:
                    ############################### Post-order and tree coloring ################
                    con.append((node.id, node.t))
                    self.canvas.itemconfig(con[0][0], fill="lightgrey", outline="black")
                    self.canvas.itemconfig(con[0][1], fill="black")
                    self.canvas.itemconfig(con[1][0], fill="green")
                    del con[0]
                    ##############################################################
                    con2.append(node.square)
                    canvas.itemconfig(con2[0], fill="lightgrey")
                    canvas.itemconfig(con2[1], fill="green")
                    del con2[0]
                    ###############################################################################

                self.canvas.after(800)
                self.canvas.update()
                #########################################3
                reverse_preord(node.right, con, con2)
                reverse_preord(node.left, con, con2)

        reverse_preord(tree, [], [])

    def controls(self, input1, input2):
        if len(input1) != len(input2):
            self.window.messagebox.after(3000, self.messagebox_disappear)
            self.window.mes.set("Number of values is not the same or Bad separator - Use , !")
            self.window.messagebox.config(bg="red")


p = Program()
